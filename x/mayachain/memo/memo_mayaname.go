package mayachain

import (
	"github.com/blang/semver"
	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/common/cosmos"
)

type ManageMAYANameMemo struct {
	MemoBase
	Name           string
	Chain          common.Chain
	Address        common.Address
	PreferredAsset common.Asset
	Expire         int64
	Owner          cosmos.AccAddress
}

func (m ManageMAYANameMemo) GetName() string            { return m.Name }
func (m ManageMAYANameMemo) GetChain() common.Chain     { return m.Chain }
func (m ManageMAYANameMemo) GetAddress() common.Address { return m.Address }
func (m ManageMAYANameMemo) GetBlockExpire() int64      { return m.Expire }

func NewManageMAYANameMemo(name string, chain common.Chain, addr common.Address, expire int64, asset common.Asset, owner cosmos.AccAddress) ManageMAYANameMemo {
	return ManageMAYANameMemo{
		MemoBase:       MemoBase{TxType: TxMAYAName},
		Name:           name,
		Chain:          chain,
		Address:        addr,
		PreferredAsset: asset,
		Expire:         expire,
		Owner:          owner,
	}
}

func (p *parser) ParseManageMAYANameMemo() (ManageMAYANameMemo, error) {
	switch {
	case p.version.GTE(semver.MustParse("1.112.0")):
		return p.ParseManageMAYANameMemoV112()
	default:
		return ParseManageMAYANameMemoV1(p.version, p.parts)
	}
}

func (p *parser) ParseManageMAYANameMemoV112() (ManageMAYANameMemo, error) {
	chain := p.getChain(2, true, common.EmptyChain)
	addr := p.getAddress(3, true, common.NoAddress, p.version)
	owner := p.getAccAddress(4, false, nil)
	preferredAsset := p.getAsset(5, false, common.EmptyAsset)
	expire := p.getInt64(6, false, 0)
	return NewManageMAYANameMemo(p.get(1), chain, addr, expire, preferredAsset, owner), p.Error()
}
