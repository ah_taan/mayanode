package coreapi

import (
	"context"
	"encoding/hex"
	"fmt"
	"time"

	apiclient "github.com/radixdlt/maya/radix_core_api_client"
	"github.com/radixdlt/maya/radix_core_api_client/models"
	"gitlab.com/mayachain/mayanode/bifrost/pkg/chainclients/radix/types"
)

type CoreApiWrapper struct {
	coreApiClient      *apiclient.RadixCoreApiClient
	network            types.Network
	httpRequestTimeout time.Duration
}

func NewCoreApiWrapper(coreApiClient *apiclient.RadixCoreApiClient, network types.Network, httpRequestTimeout time.Duration) CoreApiWrapper {
	return CoreApiWrapper{
		coreApiClient:      coreApiClient,
		network:            network,
		httpRequestTimeout: httpRequestTimeout,
	}
}

func (w *CoreApiWrapper) GetNetworkStatus() (models.NetworkStatusResponseable, error) {
	ctx, cancel := w.getContextForApiCalls()
	defer cancel()

	statusRequest := models.NewNetworkStatusRequest()
	statusRequest.SetNetwork(&w.network.LogicalName)
	statusResponse, err := w.coreApiClient.Status().NetworkStatus().Post(ctx, statusRequest, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to get network status: %w", err)
	}
	return statusResponse, nil
}

func (w *CoreApiWrapper) GetCurrentEpoch() (int64, error) {
	statusResponse, err := w.GetNetworkStatus()
	if err != nil {
		return 0, err
	}
	return *statusResponse.GetCurrentEpochRound().GetEpoch(), nil
}

func (w *CoreApiWrapper) GetCurrentStateVersion() (int64, error) {
	statusResponse, err := w.GetNetworkStatus()
	if err != nil {
		return 0, err
	}
	return *statusResponse.GetCurrentStateIdentifier().GetStateVersion(), nil
}

func (w *CoreApiWrapper) GetXrdAddress() (string, error) {
	ctx, cancel := w.getContextForApiCalls()
	defer cancel()

	networkConfiguration, err := w.coreApiClient.Status().NetworkConfiguration().Post(ctx, nil)
	if err != nil {
		return "", fmt.Errorf("failed to get network configuration: %w", err)
	}
	return *networkConfiguration.GetWellKnownAddresses().GetXrd(), nil
}

func (w *CoreApiWrapper) GetSingleTransactionAtStateVersion(stateVersion int64) (*models.CommittedTransactionable, error) {
	ctx, cancel := w.getContextForApiCalls()
	defer cancel()

	req := models.NewStreamTransactionsRequest()
	req.SetFromStateVersion(&stateVersion)
	limit := int32(1)
	req.SetLimit(&limit)
	req.SetNetwork(&w.network.LogicalName)
	sborFormatOptions := models.SborFormatOptions{}
	t := true
	sborFormatOptions.SetProgrammaticJson(&t)
	req.SetSborFormatOptions(&sborFormatOptions)

	txnsResp, err := w.coreApiClient.Stream().Transactions().Post(ctx, req, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to get txns: %w", err)
	}

	if *txnsResp.GetCount() == 0 {
		return nil, nil
	}

	txn := txnsResp.GetTransactions()[0]

	return &txn, nil
}

func (w *CoreApiWrapper) SubmitTransaction(notarizedTransactionBytes []byte) error {
	ctx, cancel := w.getContextForApiCalls()
	defer cancel()

	notarizedTransactionHex := hex.EncodeToString(notarizedTransactionBytes)
	req := models.NewTransactionSubmitRequest()
	req.SetNotarizedTransactionHex(&notarizedTransactionHex)
	req.SetNetwork(&w.network.LogicalName)

	_, err := w.coreApiClient.Transaction().Submit().Post(ctx, req, nil)
	if err != nil {
		return fmt.Errorf("failed to submit txn: %w", err)
	}

	return nil
}

func (w *CoreApiWrapper) MethodCallPreview(componentAddress string, methodName string, sborEncodedArgs []string) (models.SborDataable, error) {
	ctx, cancel := w.getContextForApiCalls()
	defer cancel()

	req := models.NewTransactionCallPreviewRequest()

	target := models.NewComponentMethodTargetIdentifier()
	target.SetComponentAddress(&componentAddress)
	target.SetMethodName(&methodName)
	targetIdentifierType := models.METHOD_TARGETIDENTIFIERTYPE
	target.SetTypeEscaped(&targetIdentifierType)
	req.SetTarget(target)

	req.SetArguments(sborEncodedArgs)

	req.SetNetwork(&w.network.LogicalName)

	resp, err := w.coreApiClient.Transaction().CallPreview().Post(ctx, req, nil)
	if err != nil {
		return nil, fmt.Errorf("method call preview failed: %w", err)
	}

	if *resp.GetStatus() != models.SUCCEEDED_TRANSACTIONSTATUS {
		return nil, fmt.Errorf("method call preview failed (status: %s)", *resp.GetStatus())
	}

	return resp.GetOutput(), nil
}

func (w *CoreApiWrapper) getContextForApiCalls() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), w.httpRequestTimeout)
}
