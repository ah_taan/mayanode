//go:build !testnet && !mocknet
// +build !testnet,!mocknet

package radixtokens

import (
	_ "embed"
)

//go:embed radix_mainnet_latest.json
var RadixTokenListRawV111 []byte
