//go:build mocknet
// +build mocknet

package mayachain

import "gitlab.com/mayachain/mayanode/common"

// Supported chains for mocknet
var SUPPORT_CHAINS_V111 = common.Chains{
	common.ARBChain,
	common.BASEChain,
	common.BTCChain,
	common.DASHChain,
	common.ETHChain,
	common.KUJIChain,
	common.THORChain,
	common.XRDChain,
	// Smoke
	// common.AVAXChain,
	// common.BCHChain,
	// common.DOGEChain,
	// common.BNBChain,
	// common.GAIAChain,
	// common.LTCChain,
}
