package tokenlist

import (
	"encoding/json"

	"gitlab.com/mayachain/mayanode/common/tokenlist/radixtokens"

	"github.com/blang/semver"
)

type RadixToken struct {
	Address  string `json:"address"`
	Symbol   string `json:"symbol"`
	Name     string `json:"name"`
	Decimals int    `json:"decimals"`
}

var radixTokenListV111 []RadixToken

func init() {
	if err := json.Unmarshal(radixtokens.RadixTokenListRawV111, &radixTokenListV111); err != nil {
		panic(err)
	}
}

func GetRadixTokenList(version semver.Version) []RadixToken {
	return radixTokenListV111
}
